﻿# Returns users who used one specified computer, and the time/date of their most recent use

# ----------- FUNCTIONS ------------- #
Function ParseTime {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $TimeString
    )
    
    $TimeString.Trim().Replace("`n","").Replace("`r","")

    If($TimeString -match "\d/\d\d/\d\d\d\d\s\d:\d\d"){
        # e.g. 2/01/2019 5:57 PM
        $Time = [datetime]::parseexact($TimeString, 'd/MM/yyyy h:mm tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf($TimeString -match "\d/\d\d/\d\d\d\d\s\d:\d\d:\d\d") {
        # e.g. 2/01/2019 5:57:34 PM
        $Time = [datetime]::parseexact($TimeString, 'd/MM/yyyy h:mm:ss tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf($TimeString -match "\d\d/\d\d/\d\d\d\d\s\d:\d\d") {
        # e.g. 22/01/2019 5:57 PM
        $Time = [datetime]::parseexact($TimeString, 'dd/MM/yyyy h:mm tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf($TimeString -match "\d\d/\d\d/\d\d\d\d\s\d:\d\d:\d\d") {
        # e.g. 22/01/2019 5:57:32 PM
        $Time = [datetime]::parseexact($TimeString, 'dd/MM/yyyy h:mm:ss tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf ($TimeString -match "\d/\d\d/\d\d\d\d\s\d\d:\d\d") {
        # e.g. 2/01/2019 11:57 PM
        $Time = [datetime]::parseexact($TimeString, 'd/MM/yyyy hh:mm tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf ($TimeString -match "\d/\d\d/\d\d\d\d\s\d\d:\d\d:\d\d") {
        # e.g. 2/01/2019 11:57:23 PM
        $Time = [datetime]::parseexact($TimeString, 'd/MM/yyyy hh:mm:ss tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf($TimeString -match "\d\d/\d\d/\d\d\d\d\s\d\d:\d\d") {
        # e.g. 12/01/2019 11:57 PM
        $Time = [datetime]::parseexact($TimeString, 'dd/MM/yyyy hh:mm tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } ElseIf($TimeString -match "\d\d/\d\d/\d\d\d\d\s\d\d:\d\d:\d\d") {
        # e.g. 12/01/2019 11:57:32 PM
        $Time = [datetime]::parseexact($TimeString, 'dd/MM/yyyy hh:mm:ss tt', `
            [Globalization.CultureInfo]::InvariantCulture)
    } Else {
        #If not matched
        $Time = ""
    }
            
    Return $Time

}

# ----------- MAIN ------------------ #

# Set logs path
$pathToLogs = "\\PATH\logs\users\"
$filemask = "*.log"
$laptop = Read-Host "Search for which computer?"
$StartTime = Get-Date

""
Write-Host "Searching for:" $laptop.ToUpper() -ForegroundColor Green
Write-Host "Total search time will be indicated when finished." -ForegroundColor Blue
Write-Host "Search started at:" $StartTime.ToString("hh:mm:ss tt") -ForegroundColor Yellow
""

$found = Get-ChildItem ($pathToLogs + $filemask) -Filter "*.log" | Select-String $laptop -List

$Results = @{}

$found | ForEach {
    $Obj = New-Object -TypeName PSObject
    
    $user = $_.Filename.ToString().Replace(".log", "")

    # Get time
    $TimeString = $_.ToString()
    $Time = (($TimeString -match "\d+/\d+/\d+\s+\d+:\d+[:\d+]\s+[A|P]M") `
        | ForEach { ParseTime $matches[0] })[1]
    
    #Format time to YYYY/MM/DD etc so sort works.
    $Time = $Time.ToString('yyyy-MM-dd hh:mm tt')

   $Results.Add($user, $Time)
}

$SortedResults = $Results.GetEnumerator() | Sort-Object Value

# Output to screen
Write-Host "History:" -ForegroundColor Yellow -NoNewline
$SortedResults
""
Write-Host "Last user:" $sortedResults[$sortedResults.Length - 1].Name -ForegroundColor Green
Write-Host "Time:" $sortedResults[$sortedResults.Length - 1].Value -ForegroundColor Green

# General output setup
$TimeNow =  Get-Date -format "yy-MM-dd-HH-mm"
$EndTime = Get-Date
$TotalTime = $EndTime - $StartTime
""
Write-Host "Search took" `
    $TotalTime.Hours "hours" `
    $TotalTime.Seconds "seconds" -Foreground Yellow


